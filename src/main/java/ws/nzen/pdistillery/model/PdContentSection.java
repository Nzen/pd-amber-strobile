/* see ../../../../../LICENSE for release details */
package ws.nzen.pdistillery.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**  */
public class PdContentSection
{

	public static final String KEY_CONTENT = "content";
	public static final String KEY_TITLE= "title";
	public static final String KEY_SUPPLEMENT = "supplement";
	private String content = "";
	private String title = "";
	private Map<String, StringlyTyped> otherContent = new HashMap<>();


	public PdContentSection(
	) {
	}


	public PdContentSection(
			String contentVal, String titleVal
	) {
		if ( contentVal == null )
			contentVal = "";
		content = contentVal;
		if ( titleVal == null )
			titleVal = "";
		title = titleVal;
	}


	public PdContentSection(
			String contentVal, String titleVal, Map<String, StringlyTyped> additional
	) {
		this( contentVal, titleVal );
		otherContent.putAll( additional );
	}


	public void addMoreContent(
			String contentType, StringlyTyped something
	) {
		otherContent.put( contentType, something );
	}


	public void addMoreContent(
			Map<String, StringlyTyped> additional
	) {
		otherContent.putAll( additional );
	}


	public Set<String> typesOfOtherContent(
	) {
		return otherContent.keySet();
	}


	public boolean hasOtherContentOfId(
			String id
	) {
		return otherContent.containsKey( id );
	}


	public StringlyTyped otherContent(
			String id
	) {
		return otherContent.get( id );
	}


	public void removeOtherContent(
			String id
	) {
		otherContent.remove( id );
	}


	public String getContent()
	{
		return content;
	}


	public void setContent( String content )
	{
		this.content = content;
	}


	public String getTitle()
	{
		return title;
	}


	public void setTitle( String title )
	{
		this.title = title;
	}


	


}


















