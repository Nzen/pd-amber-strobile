/* see ../../../../../LICENSE for release details */
package ws.nzen.pdistillery.model;

/** Represents the entire file to process */
public class PdFile
{

	public static final String KEY_PROCESSING = "pd_processing";
	public static final String KEY_CONTENT = "content";
	public PdProcessingSection remainingCompilation;
	public PdContentSection forConsumption;


	/**  */
	public PdFile(
			PdProcessingSection compileInfo,
			PdContentSection article
	) {
		if ( compileInfo == null )
			throw new NullPointerException( "Processing must not be null" );
		if ( article == null )
			throw new NullPointerException( "Content must not be null" );
		remainingCompilation = compileInfo;
		forConsumption = article;
	}


	public PdProcessingSection getProcessingInfo()
	{
		return remainingCompilation;
	}


	public PdContentSection getContent()
	{
		return forConsumption;
	}


}


















