/* see ../../../../../LICENSE for release details */

/**
Represents the assets and files utilized by PDistillery.
*/

package ws.nzen.pdistillery.model;