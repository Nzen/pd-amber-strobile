/* see ../../../../../LICENSE for release details */
package ws.nzen.pdistillery.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
To handle anything that any plugin could encourage a writer to enter, this
supports (requires the caller to handle) arbitrary structures.
*/
public class StringlyTyped
{

	public enum Is
	{
		LIST,
		MAP,
		/** for characterizing children only, never for own identity */
		MIXED,
		STRING;
	}


	private Is interpretAs;
	private String text = null;
	private List<StringlyTyped> sequentialValues = null;
	private Map<String, StringlyTyped> namedValues = null;


	public StringlyTyped(
			Is beThisType
	) {
		interpretAs = beThisType;
		if ( interpretAs == Is.STRING )
			text = "";
		else if ( interpretAs == Is.LIST )
			sequentialValues = new LinkedList<>();
		else if ( interpretAs == Is.MAP )
			namedValues = new HashMap<>();
		else if ( interpretAs == Is.MIXED )
			throw new IllegalArgumentException( "StringlyTyped must not be "+ interpretAs.name() );
	}


	public StringlyTyped(
			Is beThisType, String bla
	) {
		this( beThisType );
		text = bla;
	}


	public StringlyTyped(
			Is beThisType, List<StringlyTyped> list
	) {
		this( beThisType );
		sequentialValues.addAll( list );
	}


	public StringlyTyped(
			Is beThisType, Map<String, StringlyTyped> object
	) {
		this( beThisType );
		namedValues.putAll( object );
	}


	/** returns null if no children */
	public Is childrenAreConsistentlyOfType(
	) {
		interpretAs = beThisType;
		if ( interpretAs == Is.STRING )
			return;
		else if ( interpretAs == Is.LIST )
		{
			if ( sequentialValues.isEmpty() )
				return null;
			Is childrenAre = sequentialValues.iterator.next().getType();
			for ( StringlyTyped child : sequentialValues )
			{
				if ( child.getType() != childrenAre )
					return Is.MIXED;
			}
			return childrenAre;
		}
		else if ( interpretAs == Is.MAP )
		{
			if ( namedValues.isEmpty() )
				return null;
			Is childrenAre = namedValues.get( namedValues.keySet().iterator.next() ).getType();
			for ( String childId : namedValues.keySet() )
			{
				if ( namedValues.get( childId ).getType() != childrenAre )
					return Is.MIXED;
			}
			return childrenAre;
		}
	}


	public Is getType(
	) {
		return interpretAs;
	}


	public String getString(
	) {
		return text;
	}


	public List<StringlyTyped> getList(
	) {
		return sequentialValues;
	}


	public Map<String, StringlyTyped> getMap(
	) {
		return namedValues;
	}


	/** Destructive, this will null all fields. */
	public void setTypeAndClearCurrentValues(
			Is become
	) {
		interpretAs = become;
		text = null;
		sequentialValues = null;
		namedValues = null;
	}


	/** ignored if the type doesn't match */
	public void setString(
			String become
	) {
		if ( interpretAs == Is.STRING )
			text = become;
	}


	/** ignored if the type doesn't match */
	public void addToList(
			StringlyTyped object
	) {
		if ( interpretAs == Is.LIST )
			sequentialValues.add( object );
	}


	/** ignored if the type doesn't match */
	public void putInMap(
			String name, StringlyTyped object
	) {
		if ( interpretAs == Is.MAP )
			namedValues.put( name, object );
	}

}
























































