/* see ../../../../../LICENSE for release details */
package ws.nzen.pdistillery.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**  */
public class PdProcessingSection
{

	public static final String KEY_PLUGIN_LIST = "pd_left";
	public static final String KEY_PLUGIN_CONFIG = "plugin_config";
	private List<String> remainingPlugins = new LinkedList<>();
	private Map<String, StringlyTyped> otherContent = new HashMap<>();


	public PdProcessingSection(
			Collection<String> initialPlugins, Map<String, StringlyTyped> pluginInfo
	) {
		remainingPlugins.addAll( initialPlugins );
		otherContent.putAll( pluginInfo );
	}


	public void addMoreContent(
			Map<String, StringlyTyped> pluginInfo
	) {
		otherContent.putAll( pluginInfo );
	}


	public void appendPlugins(
			Collection<String> morePlugins
	) {
		remainingPlugins.addAll( morePlugins );
	}


	public List<String> remainingPlugins(
	) {
		return remainingPlugins; // ASK defensive copy ?
	}


	public void addMoreContent(
			String contentType, StringlyTyped something
	) {
		otherContent.put( contentType, something );
	}


	public Set<String> typesOfOtherContent(
	) {
		return otherContent.keySet();
	}


	public boolean hasOtherContentOfId(
			String id
	) {
		return otherContent.containsKey( id );
	}


	public StringlyTyped otherContent(
			String id
	) {
		return otherContent.get( id );
	}


	public void removeOtherContent(
			String id
	) {
		otherContent.remove( id );
	}



}


















