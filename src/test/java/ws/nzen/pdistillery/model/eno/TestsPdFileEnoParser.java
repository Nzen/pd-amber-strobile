/** &copy; beyondRelations, LLC */
package ws.nzen.pdistillery.model.eno;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

import ws.nzen.format.eno.DocGen;
import ws.nzen.pdistillery.model.PdContentSection;
import ws.nzen.pdistillery.model.PdFile;
import ws.nzen.pdistillery.model.PdProcessingSection;
import ws.nzen.pdistillery.model.StringlyTyped;

/**  */
class TestsPdFileEnoParser
{

	/**
	 * Test method for {@link ws.nzen.pdistillery.model.eno.PdFileEnoParser#parseEnoAsPdFile(java.lang.String)}.
	 */
	@Test
	void testParseEnoAsPdFile()
	{
		// ¶ make eno structure
		DocGen buildEno = new DocGen();
		int sectionDepth = 1;
		buildEno.section( PdFile.KEY_PROCESSING, sectionDepth );
		sectionDepth++;
		StringlyTyped processingSectionSt = new StringlyTyped( StringlyTyped.Is.MAP );
		List<String> pluginsLeftLl = new LinkedList<>();
		pluginsLeftLl.add( Integer.toString( 0 ) );
		pluginsLeftLl.add( Integer.toString( 1 ) );
		StringlyTyped pluginsLeftSt = new StringlyTyped( StringlyTyped.Is.LIST );
		buildEno.field( PdProcessingSection.KEY_PLUGIN_LIST );
		for ( String pluginName : pluginsLeftLl )
		{
			buildEno.listItem( pluginName );
			pluginsLeftSt.addToList( new StringlyTyped( StringlyTyped.Is.STRING, pluginName ) );
		}
		processingSectionSt.putInMap( PdProcessingSection.KEY_PLUGIN_LIST, pluginsLeftSt );
		// and then the free form secton
		/*
		-- using the following structure
		( 0 Map ( 2 list ( 3, 4 ) ) )
		( 1 map
			( 5 str 6 ),
			( 7 map (
				8 map ( 9 str 10 ),
					( 11 str 12 ) ) )
		)
		*/
		sectionDepth = 1;
		buildEno.section( PdFile.KEY_CONTENT, sectionDepth );
		StringlyTyped contentSectionSt = new StringlyTyped( StringlyTyped.Is.MAP );
		String title = Integer.toString( 32 );
		buildEno.field( PdContentSection.KEY_TITLE, title );
		contentSectionSt.putInMap(
				PdContentSection.KEY_TITLE,
				new StringlyTyped( StringlyTyped.Is.STRING, title ) );
		String postContent = Integer.toString( 24 );

		fail( "Not yet implemented" );
	}


	/**
	 * Test method for {@link ws.nzen.pdistillery.model.eno.PdFileEnoParser#deserializeToEno(ws.nzen.pdistillery.model.PdFile)}.
	 */
	@Test
	void testDeserializeToEno()
	{
		fail( "Not yet implemented" );
	}

}


















