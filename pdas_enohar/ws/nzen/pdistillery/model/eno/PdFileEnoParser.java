/* see ../../../../../LICENSE for release details */
package ws.nzen.pdistillery.model.eno;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ws.nzen.format.eno.DocGen;
import ws.nzen.format.eno.Eno;
import ws.nzen.format.eno.EnoElement;
import ws.nzen.format.eno.EnoType;
import ws.nzen.format.eno.Field;
import ws.nzen.format.eno.FieldList;
import ws.nzen.format.eno.FieldSet;
import ws.nzen.format.eno.ListItem;
import ws.nzen.format.eno.Multiline;
import ws.nzen.format.eno.Section;
import ws.nzen.format.eno.SetEntry;
import ws.nzen.format.eno.Value;
import ws.nzen.pdistillery.model.PdContentSection;
import ws.nzen.pdistillery.model.PdFile;
import ws.nzen.pdistillery.model.PdProcessingSection;
import ws.nzen.pdistillery.model.StringlyTyped;

/** Represents the entire file to process */
public class PdFileEnoParser
{

	private static String NAME_OF_LIST_STYLE_SECTION = "li"+ DocGen.genEscapes( 3 );


	/** Expect the exception if it isn't a valid input */
	public static PdFile parseEnoAsPdFile(
			String fileContent
	) throws IllegalArgumentException {
		Section topHierarchy = new Eno().deserialize( fileContent );
		Section contentEno = topHierarchy.optionalSection( PdFile.KEY_CONTENT );
		PdContentSection contentPd = new PdContentSection();
		if ( contentEno == null )
			throw new IllegalArgumentException( "Pd files require a content section" );
		Field contentVal = contentEno.optionalField( PdContentSection.KEY_CONTENT );
		if ( contentVal != null && contentVal.getType() == EnoType.MULTILINE )
		{
			contentPd.setContent( ((Multiline)contentVal).optionalStringValue() );
		}
		Field titleWrapper = contentEno.optionalField( PdContentSection.KEY_TITLE );
		if ( titleWrapper != null && titleWrapper.getType() == EnoType.FIELD_VALUE )
		{
			contentPd.setTitle( ((Value)titleWrapper).optionalStringValue() );
		}
		Section addendum = contentEno.optionalSection( PdContentSection.KEY_SUPPLEMENT );
		if ( addendum != null && ! addendum.elements().isEmpty() )
		{
			Map<String, StringlyTyped> contentAddendum = new HashMap<>();
			for ( EnoElement someChild : addendum.elements() )
			{
				contentAddendum.put( someChild.getName(), destructureEnoElement( someChild ) );
			}
			contentPd.addMoreContent( contentAddendum );
		}

		PdProcessingSection processing = null;
		Section pluginSection = topHierarchy.optionalSection( PdFile.KEY_PROCESSING );
		if ( pluginSection == null )
			throw new IllegalArgumentException( "Pd files require a plugin section" );
		FieldList pluginsLeftE = pluginSection.optionalList( PdProcessingSection.KEY_PLUGIN_LIST );
		if ( pluginsLeftE == null )
			throw new IllegalArgumentException( "Pd files require a plugin list" );
		List<String> pluginsLeft = new LinkedList<>();
		for ( ListItem pluginName : pluginsLeftE.items() )
		{
			String rawName = pluginName.optionalStringValue();
			if ( rawName != null )
				pluginsLeft.add( rawName );
		}
		Section pluginConfigSection = pluginSection.optionalSection( PdProcessingSection.KEY_PLUGIN_CONFIG );
		Map<String, StringlyTyped> pluginConfig = new HashMap<>();
		if ( pluginConfigSection != null && ! pluginConfigSection.elements().isEmpty() )
		{
			for ( EnoElement someChild : pluginConfigSection.elements() )
			{
				pluginConfig.put( someChild.getName(), destructureEnoElement( someChild ) );
			}
		}
		processing = new PdProcessingSection( pluginsLeft, pluginConfig );

		return new PdFile( processing, contentPd );
	}


	public static String serializeToEno(
			PdFile documentAsPf
	) {
		PdProcessingSection processingInfo = documentAsPf.getProcessingInfo();
		Section pluginSection = new Section( PdFile.KEY_PROCESSING );
		FieldList pluginNames = new FieldList( PdProcessingSection.KEY_PLUGIN_LIST );
		for ( String pluginName : processingInfo.remainingPlugins() )
			pluginNames.addItem( pluginName );
		pluginSection.addChild( pluginNames );
		Section pluginConfig = new Section( PdProcessingSection.KEY_PLUGIN_CONFIG );
		if ( ! processingInfo.typesOfOtherContent().isEmpty() )
		{
			for ( String idOfOtherContent : processingInfo.typesOfOtherContent() )
			{
				StringlyTyped something = processingInfo.otherContent( idOfOtherContent );
				if ( something.getType() == StringlyTyped.Is.STRING )
				{
					String valueOfSomething = something.getString();
					pluginConfig.addChild( valueOfSomething.contains( "\n" )
							? new Multiline( idOfOtherContent, valueOfSomething )
							: new Value( idOfOtherContent, valueOfSomething ) );
				}
				else if ( something.getType() == StringlyTyped.Is.LIST )
				{
					StringlyTyped.Is childrenAre = something.childrenAreConsistentlyOfType();
					if ( childrenAre == StringlyTyped.Is.STRING )
					{
						FieldList someList = new FieldList( idOfOtherContent );
						for ( StringlyTyped lItem : something.getList() )
							someList.addItem( lItem.getString() );
						pluginConfig.addChild( someList );
					}
					else
					{
						// sections of name NAME_OF_LIST_STYLE_SECTION
						for ( StringlyTyped listChild :  )
					}
				}
				// FIX np here todo arbitrary serialization
				/*
				list and map
				*/
			}
			pluginSection.addChild( new Section( PdProcessingSection.KEY_PLUGIN_CONFIG ) );
		}

		PdContentSection contentInfo = documentAsPf.getContent();
		Section contentSection = new Section( PdFile.KEY_CONTENT );
		Value titleWrapper = new Value( PdContentSection.KEY_TITLE, contentInfo.getTitle() );
		Multiline contentText = new Multiline( PdContentSection.KEY_CONTENT, contentInfo.getContent() );
		if ( ! contentInfo.typesOfOtherContent().isEmpty() )
		{
			// FIX np here todo same as above
		}


		throw new RuntimeException( "unimplemented FIX np here" );
	}


	private static StringlyTyped destructureEnoElement(
			EnoElement something
	) {
		StringlyTyped alternateRepresentation;
		switch ( something.getType() )
		{
			case BARE :
			case EMPTY :
				// IMPROVE this won't round trip, as it may become a field_value
				alternateRepresentation = new StringlyTyped( StringlyTyped.Is.STRING, Boolean.TRUE.toString() );
				break;
			case FIELD_EMPTY :
				someSection.put( something.getName(),
						new StringlyTyped( StringlyTyped.Is.STRING, "" ) );
				break;
			case FIELD_VALUE :
			case MULTILINE :
				alternateRepresentation = new StringlyTyped( StringlyTyped.Is.STRING, ((Value)something).optionalStringValue() );
				break;
			case FIELD_LIST :
				FieldList asEnoList = (FieldList)something;
				StringlyTyped alternateRepresentation = new StringlyTyped( StringlyTyped.Is.LIST );
				for ( ListItem value : asEnoList.items() )
					alternateRepresentation.addToList( new StringlyTyped( StringlyTyped.Is.STRING, value.optionalStringValue() ) );
				break;
				FieldSet asEnoMap = (FieldSet)something;
				StringlyTyped alternateRepresentation = new StringlyTyped( StringlyTyped.Is.MAP );
				for ( SetEntry value : asEnoMap.entries() )
					wrappedMap.putInMap(
							value.getName(),
							new StringlyTyped( StringlyTyped.Is.STRING, value.optionalStringValue() ) );
				break;
			case SECTION :
				Section asSection = (Section)something;
				if ( ! asSection.getName().equals( NAME_OF_LIST_STYLE_SECTION ) )
				{
					alternateRepresentation = new StringlyTyped( StringlyTyped.Is.LIST );
					for ( EnoElement someChild : asSection.elements() )
					{
						alternateRepresentation.addToList( destructureEnoElement( someChild ) );
					}
				}
				else
				{
					alternateRepresentation = new StringlyTyped( StringlyTyped.Is.MAP );
					for ( EnoElement someChild : asSection.elements() )
					{
						alternateRepresentation.put( someChild.getName(), destructureEnoElement( someChild ) );
					}
				}
				break;
			default :
				System.out.println( "Not handling enotype "+ something.getType() );
				break;
		}
		return alternateRepresentation;
	}


}


















